TOOLCHAIN_SUFFIX ?= "-tisdk"
LICENSE = "MIT"

require meta-toolchain-arago-tisdk.inc
require meta-toolchain-arago.bb

PR = "${INC_PR}.0"
